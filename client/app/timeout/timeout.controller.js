(function () {
    angular
        .module("sample")
        .controller("TimeoutCtrl", [TimeoutCtrl]);

    // Step 1 - Inject $timeout

    function TimeoutCtrl() {
        // Step 2 - Expect $timeout

        var self = this;

        self.greeting = "...";

        self.greet = function () {
            self.greeting = "Hello there! How are you doing today?";
            // Step 3 Use setTimeout to display a greeting after 3 seconds


            // Step 3 Use $timeout to display a greeting after 3 seconds

        };

    }
})();
