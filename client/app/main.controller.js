(function () {
    angular
        .module("sample")
        .controller("MainCtrl", [MainCtrl]);

    function MainCtrl() {
        var self = this;

        self.currentPage;

        self.pageType = {
            HTTP: "HTTP",
            TIMEOUT: "TIMEOUT",
            Q: "Q"
        };

        self.open = function (page) {
            self.currentPage = page;
        };

        self.isOpen = function (page) {
            return self.currentPage == page
        }
    }
})();