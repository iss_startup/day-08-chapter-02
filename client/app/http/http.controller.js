(function () {
    angular
        .module("sample")
        .controller("HttpCtrl", [HttpCtrl]);
    // Step 1 - Inject $http above

    function HttpCtrl() {
        // Step 2 - Expect $http
        var self = this;

        self.products = [];

        // Step 3 - use http to GET /api/products

        // Step 4 - add products to controller (self.products)

        // Step 5 - Open http.html and display products using ng-repeat

    }
})();